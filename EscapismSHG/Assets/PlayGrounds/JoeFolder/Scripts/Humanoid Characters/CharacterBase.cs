﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBase : MonoBehaviour
{
    

    protected Animator myAnimator;

    /* 
        What do we need for this Character Base script
        1. Movement
            a. Math
            b. External Input (Player or AI)
        2. Stats
            a. Health
            b. Energy
            c. Infection
            d. Movement Speed
        3. Animations
            a. Link animations with actions
            b. Hook character states with animation states
        4. Basic World Interactions
            a. Open Doors
            b. World Objects (Alarms, terminals, Lights, etc )
                We just need to prevent the NPCs from triggering any worldspace UI.
                That can be coded within the PlayerController. 
        ?. States
            a. We will need an Enum, Coroutine, or Switch Statement system?
            b. Link Animations
            c. How will we handle states and animations when they are spread between multiple scripts?
            This script will be inherited by PlayerController and AIBase / AIController.
            Will states be here? Or will they be in the higher level scripts?
    */

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
